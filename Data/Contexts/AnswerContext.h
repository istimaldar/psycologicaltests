//
// Created by istimaldar on 14.12.2018.
//

#ifndef PSYCOLOGICALTESTS_ANSWERCONTEXT_H
#define PSYCOLOGICALTESTS_ANSWERCONTEXT_H

#include "Context.h"
#include "../Models/Answer.h"

namespace PTests::Data::Contexts {
    using namespace Models;


    class AnswerContext : public Context<Answer> {
    public:
        explicit AnswerContext(bool isBinary);

        Answer* read(ifstream& file) override;

        void write(ofstream& file, const Answer& data) override;
    };


}


#endif //PSYCOLOGICALTESTS_ANSWERCONTEXT_H
