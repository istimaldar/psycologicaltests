//
// Created by istimaldar on 15.12.2018.
//

#ifndef PSYCOLOGICALTESTS_TESTCONTEXT_H
#define PSYCOLOGICALTESTS_TESTCONTEXT_H

#include "Context.h"
#include "../Models/Test.h"

namespace PTests::Data::Contexts {
    using namespace Models;

    class TestContext : public Context<Test> {
    public:
        explicit TestContext(bool isBinary);

        Test* read(ifstream& file) override;

        void write(ofstream& file, const Test& data) override;
    };
}


#endif //PSYCOLOGICALTESTS_TESTCONTEXT_H
