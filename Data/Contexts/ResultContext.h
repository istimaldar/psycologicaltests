//
// Created by istimaldar on 15.12.2018.
//

#ifndef PSYCOLOGICALTESTS_RESULTCONTEXT_H
#define PSYCOLOGICALTESTS_RESULTCONTEXT_H

#include "Context.h"
#include "../Models/Result.h"

namespace PTests::Data::Contexts {
    using namespace Models;

    class ResultContext : public Context<Result> {
    public:
        explicit ResultContext(bool isBinary);

        Result* read(ifstream& file) override;

        void write(ofstream& file, const Result& data) override;
    };
}


#endif //PSYCOLOGICALTESTS_RESULTCONTEXT_H
