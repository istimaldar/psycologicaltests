//
// Created by istimaldar on 15.12.2018.
//

#ifndef PSYCOLOGICALTESTS_QUESTIONCONTEXT_H
#define PSYCOLOGICALTESTS_QUESTIONCONTEXT_H

#include "Context.h"
#include "../Models/Question.h"

namespace PTests::Data::Contexts {
    using namespace Models;

    class QuestionContext : public Context<Question> {
    public:
        explicit QuestionContext(bool isBinary);

        Question* read(ifstream& file) override;

        void write(ofstream& file, const Question& data) override;
    };
}


#endif //PSYCOLOGICALTESTS_QUESTIONCONTEXT_H
