//
// Created by istimaldar on 15.12.2018.
//

#include "TestContext.h"
#include "../Utils/StringStreamWriter.h"
#include "../Utils/StringStreamReader.h"

namespace PTests::Data::Contexts {
    using namespace Utils;

    TestContext::TestContext(bool isBinary) : Context(isBinary) {

    }

    Test *TestContext::read(ifstream &file) {
        int id;
        string title, description;
        StringStreamReader reader = StringStreamReader();

        try {
            file >> id >> reader.withString(title) >> reader.withString(description);
            check(file);
        }
        catch (const ifstream::failure& e) {
            return nullptr;
        }
        catch (const InvalidDataFormatException& e) {
            return nullptr;
        }

        return new Test(id, title, description);
    }

    void TestContext::write(ofstream &file, const Test &data) {
        StringStreamWriter writer = StringStreamWriter();

        file << data.getId() << " " << writer.withString(data.getTitle()) << " "
             << writer.withString(data.getDescription());

        seal(file);
    }
}