//
// Created by istimaldar on 12/13/2018.
//

#ifndef PSYCOLOGICALTESTS_WRITER_H
#define PSYCOLOGICALTESTS_WRITER_H

#include <iostream>
#include <fstream>
#include "../../Infrastructure/InvalidDataFormatException.h"


namespace PTests::Data::Contexts {
    using namespace std;
    using namespace Infrastructure;

    template <typename T>
    class Context {
    public:
        explicit Context(bool isBinary) : mIsBinary(isBinary) {}

        virtual T* read(ifstream& file) = 0;

        virtual void write(ofstream& file, const T& data) = 0;
    protected:
        void seal(ofstream& file);

        void check(ifstream& file);

        bool mIsBinary = false;
    private:
        const string RECORDS_SEPARATOR = "\n";
    };

    template<typename T>
    void Context<T>::seal(ofstream &file) {
        size_t size = RECORDS_SEPARATOR.length();

        file.write(RECORDS_SEPARATOR.c_str(), size);
    }

    template<typename T>
    void Context<T>::check(ifstream &file) {
        size_t size = RECORDS_SEPARATOR.length();
        char buffer[size + 1];
        buffer[size] = '\0';

        file.read(buffer, size);
        string result = buffer;

        if (result != RECORDS_SEPARATOR) {
            throw InvalidDataFormatException("Records in data file has wrong format");
        }
    }
}


#endif //PSYCOLOGICALTESTS_WRITER_H
