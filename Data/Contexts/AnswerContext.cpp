//
// Created by istimaldar on 14.12.2018.
//

#include "AnswerContext.h"
#include "../Utils/StringStreamWriter.h"
#include "../Utils/StringStreamReader.h"


namespace PTests::Data::Contexts {
    using namespace Utils;

    AnswerContext::AnswerContext(bool isBinary) : Context(isBinary) {

    }

    Answer *AnswerContext::read(ifstream &file) {
        int id, answerNumber, questionId;
        bool isCorrectAnswer;
        string text;
        StringStreamReader reader = StringStreamReader();

        auto exceptions = file.exceptions();
        file.exceptions(ifstream::badbit | ifstream::eofbit);
        try {
            file >> id >> answerNumber >> isCorrectAnswer >> questionId >> reader.withString(text);
            check(file);
        }
        catch (const ifstream::failure& e) {
            file.exceptions(exceptions);
            return nullptr;
        }
        catch (const InvalidDataFormatException& e) {
            file.exceptions(exceptions);
            return nullptr;
        }
        file.exceptions(exceptions);

        return new Answer(id, answerNumber, isCorrectAnswer, questionId, text);
    }

    void AnswerContext::write(ofstream &file, const Answer &data) {
        StringStreamWriter writer = StringStreamWriter();

        file << data.getId() << " " << data.getAnswerNumber() << " " << data.isCorrect() << " " << data.getQuestionId()
             << " " << writer.withString(data.getText());

        seal(file);
    }
}
