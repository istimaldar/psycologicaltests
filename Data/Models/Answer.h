//
// Created by istimaldar on 13.12.2018.
//

#ifndef PSYCOLOGICALTESTS_ANSWER_H
#define PSYCOLOGICALTESTS_ANSWER_H

#include "Model.h"

namespace PTests::Data::Models {
    class Answer : public Model {
    public:
        explicit Answer(int id);

        Answer(int id, int answerNumber, bool isCorrectAnswer, int questionId, const string& text);

        int getAnswerNumber() const;

        void setAnswerNumber(int answerNumber);

        bool isCorrect() const;

        void setIsCorrect(bool isCorrect);

        int getQuestionId() const;

        void setQuestionId(int questionId);

        const string &getText() const;

        void setText(const string &text);
    private:
        int mAnswerNumber = 1;

        bool mIsCorrect = false;

        int mQuestionId = -1;

        string mText = "";
    };
}


#endif //PSYCOLOGICALTESTS_ANSWER_H
