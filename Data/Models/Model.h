//
// Created by istimaldar on 13.12.2018.
//

#ifndef PSYCOLOGICALTESTS_MODEL_H
#define PSYCOLOGICALTESTS_MODEL_H

#include <istream>
namespace PTests::Data::Models {
    using namespace std;

    class Model {
    public:
        explicit Model(int id);

        int getId() const ;

        void setId(int value);

        virtual ~Model() = 0;
    protected:
        int mId;
    };
}

#endif //PSYCOLOGICALTESTS_MODEL_H
