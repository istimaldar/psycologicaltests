//
// Created by istimaldar on 13.12.2018.
//

#include "Model.h"
namespace PTests::Data::Models {
    Model::Model(int id) : mId(id) {

    }

    int Model::getId() const {
        return mId;
    }

    void Model::setId(int value) {
        mId = value;
    }

    Model::~Model() = default;
}