//
// Created by istimaldar on 15.12.2018.
//

#include "StringStreamReader.h"

namespace PTests::Data::Utils {
    StringStreamReader &StringStreamReader::withString(string &target) {
        content = &target;
        return *this;
    }

    istream &operator>>(istream &stream, StringStreamReader &object) {
        if (object.content != nullptr) {
            size_t size;
            stream >> size;

            if (stream.good()) {
                char buffer[size + 1];
                buffer[size] = '\0';

                stream.ignore();
                stream.read(buffer, size * sizeof(char));

                (*object.content) = buffer;
            }
        }

        object.content = nullptr;

        return stream;
    }
}