//
// Created by istimaldar on 15.12.2018.
//

#ifndef PSYCOLOGICALTESTS_STRINGSTREAMMANIPULATOR_H
#define PSYCOLOGICALTESTS_STRINGSTREAMMANIPULATOR_H

#include <iostream>
#include <fstream>

namespace PTests::Data::Utils {
    using namespace std;

    class StringStreamReader {
    public:
        StringStreamReader& withString(string& target);

        friend istream& operator>>(istream& stream, StringStreamReader& object);
    private:
        string* content;
    };
}


#endif //PSYCOLOGICALTESTS_STRINGSTREAMMANIPULATOR_H
