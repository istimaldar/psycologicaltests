//
// Created by istimaldar on 15.12.2018.
//

#ifndef PSYCOLOGICALTESTS_STRINGSTREAMREADER_H
#define PSYCOLOGICALTESTS_STRINGSTREAMREADER_H

#include <iostream>
#include <fstream>

namespace PTests::Data::Utils {
    using namespace std;
    class StringStreamWriter {
    public:
        StringStreamWriter& withString(const string& target);

        friend ostream& operator<<(ostream& stream, StringStreamWriter& object);
    private:
        const string*  content;
    };
}


#endif //PSYCOLOGICALTESTS_STRINGSTREAMREADER_H
