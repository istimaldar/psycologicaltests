//
// Created by istimaldar on 15.12.2018.
//

#include "StringStreamWriter.h"

namespace PTests::Data::Utils {
    StringStreamWriter &StringStreamWriter::withString(const string &target) {
        content = &target;
    }

    ostream &operator<<(ostream &stream, StringStreamWriter &object) {
        if (object.content != nullptr) {
            stream << object.content->length() << " " << (*object.content);
        }

        object.content = nullptr;

        return stream;
    }
}