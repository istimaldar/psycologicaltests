//
// Created by istimaldar on 15.12.2018.
//

#include "Application.h"
#include <iostream>
#include <functional>
#include "../Utils/Utility.h"

namespace PTests::Infrastructure {
    using namespace std;
    using namespace Utils;

    const size_t BUFFERS_SIZE = 65536;

    Application *Application::mInstance = nullptr;

    Application &Application::getInstance() {
        if (mInstance == nullptr) {
            mInstance = new Application();
        }
        return *mInstance;
    }

    void Application::clearConsole() {
#if defined(_WIN32) || defined(_WIN64)
        system("cls");
#else
        system("clear");
#endif
    }

    string Application::readMultiline() {
        string result;
        do {
            string buffer;
            getline(cin, buffer);
            if (buffer.empty()) {
                result.pop_back();
                break;
            }
            result += buffer + '\n';
        } while (true);
        return result;
    }

    void Application::showMenu(string_view title, list<pair<string, function<void()>>> &menu) {
        clearConsole();
        cout << title << endl;
        int number = 1;
        for (auto iterator = menu.begin(); iterator != menu.end(); iterator++, number++) {
            auto menuItem = *iterator;
            cout << number << ". " << menuItem.first << endl;
        }
        int result = 0;
        do {
            cout << "Select menu item: ";
            cin >> result;
            if (result >= 1 && result <= number) {
                auto iterator = menu.begin();
                while (result > 1) {
                    iterator++;
                    result--;
                }
                (*iterator).second();
                break;
            } else {
                cout << "Invalid input. Please, enter number between 1 and " << number << "." << endl;
                cin.clear();
                cin.ignore(INT16_MAX, '\n');
            }
        } while (true);
    }

    template<typename T>
    void Application::listItems(ListingPurpose purpose, Service<T> *service, ListingMessages messages,
                                function<string(T)> promptFunction, function<bool(const T &)> filterFunction) {
        static_assert(is_base_of<Model, T>::value, "Type parameter must br child of Model");
        clearConsole();
        cout << messages.greeting << endl;
        list<T *> *items = service->read(true);

        int number = 0;
        if (!items->empty()) {
            for (auto item : (*items)) {
                number++;
                cout << number << ". " << promptFunction(*item) << endl;
            }
        } else {
            cout << messages.empty << endl;
        }

        number++;
        cout << number << ". Back" << endl;

        number++;
        cout << number << ". Exit" << endl;

        function<void(int)> itemAction = resolveListAction<T>(purpose);

        int result;
        do {
            cout << "Select menu item: ";
            cin >> result;
            if (result > 0 & result <= number) {
                break;
            } else {
                cout << "Invalid input. Please, enter number between 1 and " << number << "." << endl;
            }
        } while (true);

        function<void()> menuItems[] = {[this, itemAction, result, items]() {
            int i = result - 1;
            auto itemsIterator = items->begin();
            while (i > 0 && itemsIterator != items->end()) {
                itemsIterator++;
                i--;
            }
            auto *typedItem = dynamic_cast<Model *>((*itemsIterator));
            itemAction(typedItem->getId());
        }, {[]() {}},
                                        {[]() { exit(0); }}};
        int functionNumber = (result - number + 3) > 0 ? result - number + 2 : 0;

        menuItems[functionNumber]();

        Utility::disposeList(*items);
    }

    void Application::runTest(int testId) {

    }

    void Application::createTest() {
        string title, description;

        clearConsole();
        cout << "Creating new test..." << endl;

        cin.clear();
        cin.ignore(INT16_MAX, '\n');

        cout << "Enter test title: " << endl;
        cout.flush();
        getline(cin, title);

        cin.clear();

        cout << "Enter test description: " << endl;
        cout.flush();
        description = readMultiline();

        title = title.empty() ? "NOTITLE" : title;
        description = description.empty() ? "NODESCRIPTION" : description;

        auto *test = new Test(-1, title, description);
        mTestService->create(*test);
        delete test;
    }

    void Application::mainMenu() {
        list<pair<string, function<void()>>> menu = {{"Launch test", [this]() {
            listItems<Test>(ListingPurpose::Run, mTestService,
                            {"Select test to run...", "No tests yet. Create some tests first."},
                            [](Test test) { return test.getTitle(); });
        }},
                                                     {"Add new test", [this]() { createTest(); }},
                                                     {"Edit test", [this]() {
                                                         listItems<Test>(ListingPurpose::Edit, mTestService,
                                                                         {"Select test to edit...",
                                                                          "No tests yet. Create some tests first."},
                                                                         [](Test test) { return test.getTitle(); });
                                                     }},
                                                     {"Delete test", [this]() {
                                                         listItems<Test>(ListingPurpose::Delete, mTestService,
                                                                         {"Select test to delete...",
                                                                          "No tests yet. Create some tests first."},
                                                                         [](Test test) { return test.getTitle(); });
                                                     }},
                                                     {"Exit", []() { exit(0); }}};
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
        do {
            showMenu("Welcome to test application. To begin work with tests select one item from list below.", menu);
        } while (true);
#pragma clang diagnostic pop

    }

    int Application::run() {
        mainMenu();
        return 0;
    }

    void Application::editTest(int testId) {
        list<pair<string, function<void()>>> menu = {{"Edit test record", []() {}},
                                                     {"Add question",     [this, testId]() { createQuestion(testId); }},
                                                     {"Edit question",    [this, testId]() {
                                                         listItems<Question>(ListingPurpose::Edit, mQuestionService,
                                                                             {"Select question to edit...",
                                                                              "No questions yet. Create some questions first."},
                                                                             [](Question question) { return question.getText(); },
                                                                             [testId](const Question &question) {
                                                                                 return question.getTestId() == testId;
                                                                             });
                                                     }},
                                                     {"Delete question",  [this, testId]() {
                                                         listItems<Question>(ListingPurpose::Delete, mQuestionService,
                                                                             {"Select question to delete...",
                                                                              "No questions yet. Create some questions first."},
                                                                             [](Question question) { return question.getText(); },
                                                                             [testId](const Question &question) {
                                                                                 return question.getTestId() == testId;
                                                                             });
                                                     }},
                                                     {"Add result",       [this, testId]() { createResult(testId); }},
                                                     {"Edit result",      [this, testId]() {
                                                         listItems<Result>(ListingPurpose::Edit, mResultService,
                                                                           {"Select result to edit...",
                                                                            "No results yet. Create some results first."},
                                                                           [](Result result) { return result.getResultName(); },
                                                                           [testId](const Result &result) {
                                                                               return result.getTestId() == testId;
                                                                           });
                                                     }},
                                                     {"Delete result",    [this, testId]() {
                                                         listItems<Result>(ListingPurpose::Delete, mResultService,
                                                                           {"Select result to delete...",
                                                                            "No results yet. Create some results first."},
                                                                           [](Result result) { return result.getResultName(); },
                                                                           [testId](const Result &result) {
                                                                               return result.getTestId() == testId;
                                                                           });
                                                     }},
                                                     {"Back",             []() {}},
                                                     {"Exit",             []() { exit(0); }}};
        showMenu("Welcome to test application. To begin work with tests select one item from list below.", menu);
    }

    void Application::deleteTest(int testId) {
        mTestService->deleteElements([testId](const Test &test) { return test.getId() == testId; });
    }

    template<typename T>
    function<void(int)> Application::resolveListAction(ListingPurpose listingPurpose) {
        return function<void(int)>();
    }

    void Application::createQuestion(int testId) {
        list<Question *> *currentTestQuestions = mQuestionService->read(
                [testId](const Question &question) { return question.getTestId() == testId; });
        auto questionNumber = static_cast<int>(currentTestQuestions->size() + 1);
        Utility::disposeList(*currentTestQuestions);

        clearConsole();
        cout << "Creating new question..." << endl;

        cin.clear();
        cin.ignore(INT16_MAX, '\n');

        string text;
        cout << "Enter question text: " << endl;
        cout.flush();
        text = readMultiline();

        auto *question = new Question(-1, questionNumber, testId, text);
        mQuestionService->create(*question);
        delete question;
    }

    void Application::createResult(int testId) {
        list<Result *> *resultsForTest = mResultService->read(
                [testId](const Result &result) { return result.getTestId() == testId; });
        auto takenIntervals = list<pair<int, int>>();
        for (auto result : (*resultsForTest)) {
            takenIntervals.push_back(result->getInterval());
        }
        Utility::disposeList(*resultsForTest);

        list<Question *> *questionsInTest = mQuestionService->read(
                [testId](const Question &question) { return question.getTestId() == testId; });
        auto numberOfQuestions = static_cast<int>(questionsInTest->size());
        Utility::disposeList(*questionsInTest);


        clearConsole();
        cout << "Creating new result..." << endl;


        cout << "Existing intervals: ";
        for (auto interval : takenIntervals) {
            cout << "[" << interval.first << "; " << interval.second << "] ";
        }
        cout << endl;

        cin.clear();

        int lowerBound = -1;
        cout << "Enter lower bound: ";
        cout.flush();
        do {
            cin >> lowerBound;
            if (validateResultInterval(lowerBound, takenIntervals)) {
                if (lowerBound >= 0 && lowerBound <= numberOfQuestions) {
                    break;
                } else {
                    cout << "Invalid input. Please, enter number between 0 and " << numberOfQuestions << "." << endl;
                }
            }
        } while (true);

        cin.clear();

        int upperBound = -1;
        cout << "Enter lower bound: ";
        cout.flush();
        do {
            cin >> upperBound;
            if (validateResultInterval(upperBound, takenIntervals)) {
                if (upperBound >= lowerBound && upperBound <= numberOfQuestions) {
                    break;
                } else {
                    cout << "Invalid input. Please, enter number between " << lowerBound << " and " << numberOfQuestions
                         << "." << endl;
                }
            }
        } while (true);

        cin.clear();
        cin.ignore(INT16_MAX, '\n');

        cout << "Enter result name: " << endl;
        cout.flush();
        string name;
        getline(cin, name);

        cin.clear();

        cout << "Enter result description: " << endl;
        cout.flush();
        string description;
        description = readMultiline();

        auto *testResult = new Result(-1, pair<int, int>(lowerBound, upperBound), testId, name, description);
        mResultService->create(*testResult);
        delete testResult;
    }

    bool Application::validateResultInterval(int value, list<pair<int, int>> &intervals) {
        for (auto interval : intervals) {
            if (value >= interval.first && value <= interval.second) {
                cout << "Invalid value! Value is intersecting with interval [" << interval.first << "; "
                     << interval.second << "].";
                return false;
            }
        }
        return true;
    }

    void Application::deleteResult(int resultId) {
        mResultService->deleteElements([resultId](const Result &result) { return result.getId() == resultId; });
    }

    void Application::deleteQuestion(int questionId) {
        mQuestionService->deleteElements(
                [questionId](const Question &question) { return question.getId() == questionId; });
    }

    void Application::editResult(int resultId) {

    }

    void Application::editQuestion(int questionId) {
        list<pair<string, function<void()>>> menu = {{"Edit test record", []() {}},
                                                     {"Add answer",       [this, questionId]() { createAnswer(questionId); }},
                                                     {"Edit answer",      [this, questionId]() {
                                                         listItems<Answer>(ListingPurpose::Edit, mAnswerService,
                                                                             {"Select answer to edit...",
                                                                              "No answers yet. Create some answers first."},
                                                                           [](Answer answer) { return answer.getText(); },
                                                                           [questionId](const Answer &answer) {
                                                                               return answer.getQuestionId() == questionId;
                                                                           });
                                                     }},
                                                     {"Delete answer",    [this, questionId]() {
                                                         listItems<Answer>(ListingPurpose::Delete, mAnswerService,
                                                                             {"Select answer to delete...",
                                                                              "No answers yet. Create some answers first."},
                                                                             [](Answer answer) { return answer.getText(); },
                                                                             [questionId](const Answer &answer) {
                                                                                 return answer.getQuestionId() == questionId;
                                                                             });
                                                     }},
                                                     {"Back",             []() {}},
                                                     {"Exit",             []() { exit(0); }}};
        showMenu("Welcome to test application. To begin work with tests select one item from list below.", menu);
    }

    void Application::createAnswer(int questionId) {
        list<Answer *> *questionAnswers = mAnswerService->read(
                [questionId](const Answer &answer) { return answer.getQuestionId() == questionId; });
        auto answerNumber = static_cast<int>(questionAnswers->size() + 1);
        Utility::disposeList(*questionAnswers);

        clearConsole();
        cout << "Creating new answer..." << endl;

        cin.clear();
        cin.ignore(INT16_MAX, '\n');

        bool isCorrect;
        do {
            string input;
            cout << "Is answer correct (y/n): ";
            cin >> input;
            if (input == "y") {
                isCorrect = true;
                break;
            } else if (input == "n") {
                isCorrect = false;
                break;
            } else {
                cout << "Incorrect input. Please, enter y or n." << endl;
            }
        } while (true);

        cin.clear();
        cout << "Enter answer text:" << endl;
        cout.flush();
        cin.ignore(INT16_MAX, '\n');
        string text = readMultiline();

        auto *answer = new Answer(-1, answerNumber, isCorrect, questionId, text);
        mAnswerService->create(*answer);
        delete answer;
    }

    bool Application::runQuestion(const Question& question) {
        bool exit = false;




    }

    template<>
    function<void(int)> Application::resolveListAction<Test>(ListingPurpose listingPurpose) {
        switch (listingPurpose) {
            case ListingPurpose::Delete:
                return [this](int id) { deleteTest(id); };
            case ListingPurpose::Edit:
                return [this](int id) { editTest(id); };
            case ListingPurpose::Run:
                return [this](int id) { runTest(id); };
        }
    }

    template<>
    function<void(int)> Application::resolveListAction<Question>(ListingPurpose listingPurpose) {
        switch (listingPurpose) {
            case ListingPurpose::Delete:
                return [this](int id) { deleteQuestion(id); };
            case ListingPurpose::Edit:
                return [this](int id) { editQuestion(id); };
            case ListingPurpose::Run:
                return [](int id) {};
        }
    }

    template<>
    function<void(int)> Application::resolveListAction<Result>(ListingPurpose listingPurpose) {
        switch (listingPurpose) {
            case ListingPurpose::Delete:
                return [this](int id) { deleteResult(id); };
            case ListingPurpose::Edit:
                return [this](int id) { editResult(id); };
            case ListingPurpose::Run:
                return [](int id) {};
        }
    }

    Application::Application() = default;

}