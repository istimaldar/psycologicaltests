//
// Created by istimaldar on 15.12.2018.
//

#ifndef PSYCOLOGICALTESTS_INVALIDDATAFORMATEXCEPTION_H
#define PSYCOLOGICALTESTS_INVALIDDATAFORMATEXCEPTION_H
#include <stdexcept>

using namespace std;

namespace PTests::Infrastructure {
    class InvalidDataFormatException : public runtime_error {
    public:
        explicit InvalidDataFormatException(const string&  what_arg);
    };
}


#endif //PSYCOLOGICALTESTS_INVALIDDATAFORMATEXCEPTION_H
