//
// Created by istimaldar on 16.12.2018.
//

#ifndef PSYCOLOGICALTESTS_STORAGE_H
#define PSYCOLOGICALTESTS_STORAGE_H

#include <string>
#include <typeinfo>
#include <unordered_map>
#include "../Data/Models/Answer.h"
#include "../Data/Models/Question.h"
#include "../Data/Models/Test.h"
#include "../Data/Models/Result.h"

namespace PTests::Infrastructure {
    using namespace std;
    using namespace PTests::Data::Models;

    class Storage {
    public:
        static Storage &getInstance();

        const string getFileName(const type_info& type) const;
    private:
        static Storage *mInstance;

        Storage();

        const unordered_map<string, string> mFileNames = {{typeid(Answer).name(),   "Answers"},
                                                          {typeid(Question).name(), "Question"},
                                                          {typeid(Result).name(),   "Results"},
                                                          {typeid(Test).name(), "Test"}};
    };
}


#endif //PSYCOLOGICALTESTS_STORAGE_H
