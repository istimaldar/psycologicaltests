//
// Created by istimaldar on 15.12.2018.
//

#include "InvalidDataFormatException.h"

namespace PTests::Infrastructure {

    InvalidDataFormatException::InvalidDataFormatException(const string &what_arg) : runtime_error(what_arg) {

    }
}