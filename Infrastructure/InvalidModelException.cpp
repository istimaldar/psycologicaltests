//
// Created by istimaldar on 16.12.2018.
//

#include "InvalidModelException.h"

namespace PTests::Infrastructure {

    InvalidModelException::InvalidModelException(const string &what_arg) : runtime_error(what_arg) {

    }
}