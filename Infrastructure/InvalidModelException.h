//
// Created by istimaldar on 16.12.2018.
//

#ifndef PSYCOLOGICALTESTS_INVALIDMODELEXCEPTION_H
#define PSYCOLOGICALTESTS_INVALIDMODELEXCEPTION_H

#include <stdexcept>

using namespace std;

namespace PTests::Infrastructure {
    class InvalidModelException : public runtime_error {
    public:
        explicit InvalidModelException(const string&  what_arg);
    };
}


#endif //PSYCOLOGICALTESTS_INVALIDMODELEXCEPTION_H
