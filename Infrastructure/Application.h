//
// Created by istimaldar on 15.12.2018.
//

#ifndef PSYCOLOGICALTESTS_APPLICATION_H
#define PSYCOLOGICALTESTS_APPLICATION_H

#include <string_view>
#include <string>
#include <unordered_map>
#include "../Data/Models/Answer.h"
#include "../Data/Models/Question.h"
#include "../Data/Models/Test.h"
#include "../Data/Models/Result.h"
#include "../Services/TestService.h"
#include "../Services/QuestionService.h"
#include "../Services/ResultService.h"
#include "../Services/AnswerService.h"

namespace PTests::Infrastructure {
    using namespace std;
    using namespace Data::Models;
    using namespace Services;

    enum ListingPurpose {
        Run = 1,
        Edit = 2,
        Delete = 3
    };

    struct ListingMessages {
        string_view greeting;
        string_view empty;
    };

    class Application {
    public:
        static Application &getInstance();

        int run();

    private:
        static Application *mInstance;

        Application();

        TestService *mTestService = new TestService(false);

        ResultService *mResultService = new ResultService(false);

        QuestionService *mQuestionService = new QuestionService(false);

        AnswerService *mAnswerService = new AnswerService(false);

        void mainMenu();

        void createTest();

        template<typename T>
        void listItems(ListingPurpose purpose, Service<T> *service, ListingMessages messages,
                       function<string(T)> promptFunction, function<bool(const T &)> filterFunction = [](const T&) { return true; });

        template<typename T>
        function<void(int)> resolveListAction(ListingPurpose listingPurpose);

        void showMenu(string_view title, list<pair<string, function<void()>>> &menu);

        void clearConsole();

        void runTest(int testId);

        void editTest(int testId);

        void deleteTest(int testId);

        void createQuestion(int testId);

        void createResult(int testId);

        void deleteResult(int resultId);

        void deleteQuestion(int questionId);

        void editResult(int resultId);

        void editQuestion(int questionId);

        void createAnswer(int questionId);

        bool runQuestion(const Question& question);

        bool validateResultInterval(int value, list<pair<int, int>> &intervals);

        string readMultiline();
    };
}


#endif //PSYCOLOGICALTESTS_APPLICATION_H
