//
// Created by istimaldar on 15.12.2018.
//

#ifndef PSYCOLOGICALTESTS_UTILS_H
#define PSYCOLOGICALTESTS_UTILS_H

#include <list>

namespace PTests::Utils {
    using namespace std;

    class Utility {
    public:
        template <typename T>
        static void disposeList(list<T*>& disposable);
    };

    template<typename T>
    void Utility::disposeList(list<T *> &disposable) {
        for (auto element : disposable) {
            if (element != nullptr) {
                delete element;
            }
        }
        disposable.clear();
        delete &disposable;
    }
}


#endif //PSYCOLOGICALTESTS_UTILS_H
