//
// Created by istimaldar on 15.12.2018.
//

#ifndef PSYCOLOGICALTESTS_TESTSERVICE_H
#define PSYCOLOGICALTESTS_TESTSERVICE_H

#include "Service.h"
#include "../Data/Models/Test.h"
#include "QuestionService.h"
#include "ResultService.h"

namespace PTests::Services {
    using namespace Data::Models;

    class TestService : public Service<Test> {
    public:
        explicit TestService(bool isBinary);
    private:
        ValidationResult* validateModel(const Test& model) override;

        void includeDependent(Test &model) override;

        bool mIsBinary;
    };
}


#endif //PSYCOLOGICALTESTS_TESTSERVICE_H
