//
// Created by istimaldar on 15.12.2018.
//

#include "QuestionService.h"

#include "../Data/Contexts/QuestionContext.h"

namespace PTests::Services {

    QuestionService::QuestionService(bool isBinary) : Service(new QuestionContext(isBinary)), mIsBinary(isBinary) {

    }

    ValidationResult *QuestionService::validateModel(const Question &model) {
        return new ValidationResult{true, ""};
    }

    void QuestionService::includeDependent(Question &model) {
        int questionId = model.getId();
        AnswerService answerService = AnswerService(mIsBinary);

        model.addAnswers(*answerService.read([questionId](const Answer& answer) { return answer.getQuestionId() == questionId; }));
    }
}
