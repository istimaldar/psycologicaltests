//
// Created by istimaldar on 15.12.2018.
//

#ifndef PSYCOLOGICALTESTS_RESULTSERVICE_H
#define PSYCOLOGICALTESTS_RESULTSERVICE_H

#include "Service.h"
#include "../Data/Models/Result.h"

namespace PTests::Services {
    using namespace Data::Models;

    class ResultService : public Service<Result> {
    public:
        explicit ResultService(bool isBinary);

    private:
        ValidationResult* validateModel(const Result& model) override;

        void includeDependent(Result &model) override;

        bool mIsBinary;
    };

}


#endif //PSYCOLOGICALTESTS_RESULTSERVICE_H
