//
// Created by istimaldar on 15.12.2018.
//

#ifndef PSYCOLOGICALTESTS_ANSWERSERVICE_H
#define PSYCOLOGICALTESTS_ANSWERSERVICE_H

#include "Service.h"
#include "../Data/Models/Answer.h"
#include "../Data/Contexts/AnswerContext.h"

namespace PTests::Services {
    using namespace Data::Models;

    class AnswerService : public Service<Answer> {
    public:
        explicit AnswerService(bool isBinary);
    private:
        ValidationResult* validateModel(const Answer& model) override;

        void includeDependent(Answer &model) override;

        bool mIsBinary;
    };
}


#endif //PSYCOLOGICALTESTS_ANSWERSERVICE_H
