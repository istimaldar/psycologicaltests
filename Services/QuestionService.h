//
// Created by istimaldar on 15.12.2018.
//

#ifndef PSYCOLOGICALTESTS_QUESTIONSERVICE_H
#define PSYCOLOGICALTESTS_QUESTIONSERVICE_H

#include "../Data/Models/Question.h"
#include "Service.h"
#include "AnswerService.h"

namespace PTests::Services {
    using namespace Data::Models;

    class QuestionService : public Service<Question> {
    public:
        explicit QuestionService(bool isBinary);
    private:
        ValidationResult* validateModel(const Question& model) override;

        void includeDependent(Question &model) override;

        bool mIsBinary;
    };
}


#endif //PSYCOLOGICALTESTS_QUESTIONSERVICE_H
