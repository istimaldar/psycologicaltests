//
// Created by istimaldar on 15.12.2018.
//

#include "AnswerService.h"

namespace PTests::Services {
    using namespace PTests::Infrastructure;

    AnswerService::AnswerService(bool isBinary) : Service(new AnswerContext(isBinary)), mIsBinary(isBinary) {

    }

    ValidationResult *AnswerService::validateModel(const Answer &model) {
        return new ValidationResult{true, ""};
    }

    void AnswerService::includeDependent(Answer &model) {

    }
}