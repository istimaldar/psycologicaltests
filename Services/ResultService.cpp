//
// Created by istimaldar on 15.12.2018.
//

#include "ResultService.h"

#include "../Data/Contexts/ResultContext.h"

namespace PTests::Services {

    ResultService::ResultService(bool isBinary) : Service(new ResultContext(isBinary)), mIsBinary(isBinary) {

    }

    ValidationResult *ResultService::validateModel(const Result &model) {
        return new ValidationResult{true, ""};
    }

    void ResultService::includeDependent(Result &model) {

    }
}