#include "Infrastructure/Application.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace PTests::Infrastructure;
using namespace std;

int main() {
   return Application::getInstance().run();
}